import { APIurl } from '../variables';
import axios from 'axios';
import { checkUser } from './Profile';

const url = `${APIurl}/userAuth`;
// console.log(`${url}/singup`);

export const singup = (data,empOrstd) => {
    // console.log('data : -> ',data);
    return axios({
        url: `/singup/${empOrstd}`,
        baseURL: url,
        method: 'POST',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        data: data
    })
        .then(responce => responce.data)
        .catch(error => {
            // console.log(error);
            let errorResSjson;
            let errorjson;
            // console.log('error line 75 -> ', error);
            if (error.responce) {
                console.log('error.responce -> ', error.responce)
            }
            else if (error.request) {
                // console.log('error.request -> ', error.request)
                errorResSjson = error.request.response  // '{"key":"value"}' sring type json
                errorjson = JSON.parse(errorResSjson)
            }
            else {
                console.log('error.message -> ', error.message)
            }
            return { error: errorjson.error }
        });
}

export const login = (data,usertype) => {   // hare is usertype is student or employer
    // console.log('data : -> ',data);
    return axios({
        url: `/login/${usertype}`,
        baseURL: url,
        method: 'POST',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        data: data
    })
        .then(responce => responce)
        .catch(error => {
            // console.log(error);
            let errorResSjson;
            let errorjson;
            // console.log('error line 75 -> ', error);
            if (error.responce) {
                console.log('error.responce -> ', error.responce)
            }
            else if (error.request) {
                // console.log('error.request -> ', error.request)
                errorResSjson = error.request.response  // '{"key":"value"}' sring type json
                errorjson = JSON.parse(errorResSjson)
            }
            else {
                console.log('error.message -> ', error.message)
            }
            return { error: errorjson }
        });
}
export const logout = () => {
    return axios({
        url:'/logout',
        baseURL:url,
        method:'GET',
        headers:{
            "Content-Type": "application/json"
        }
    }).then(responce => {
        if(responce.data){
            console.log('data ->',responce.data);
            if(typeof window !== undefined) localStorage.removeItem('t_k')
            return responce.data

        }
    })
    .catch(error => console.log('error catch -> ',error))
}

// authenticate user 
export const authenticate = (data) => {
    // console.log('data -> +',data);
    if(typeof window !== undefined){
        window.localStorage.setItem('t_k',JSON.stringify(data))
    }
    return;
}

// check authenticate or not  
export const isAuthenticate = () => {
    if(typeof window == undefined){
        return false;
    }
    if(localStorage.getItem('t_k')){
        return JSON.parse(localStorage.getItem('t_k'))
    }else{
        return false;
    }
}

