import React, { Component } from 'react';

class Dashboard extends Component {
    render() {
        return (
            <div className='bg-white p-3'>
                <h1 className='secund-heading text-capitalize fs-4'>Deshboard</h1>
                {
                    this.props.post ? (
                        <div>Yes</div>
                    ) : (
                        <div>
                            No activity found
                        </div>
                    )
                }
            </div>
        );
    }
}

export default Dashboard;
