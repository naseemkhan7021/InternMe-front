import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { isAuthenticate } from '../../API/Autho';
import { addDetails, deleteDetails } from '../../API/MoreDetial';

class WorkExpss extends Component {
    constructor(params) {
        super(params)
        this.state = {
            workExpss:this.props.work,
            current: false,
            experianceType:'',
            projectTitle:'',
            role: '',
            company: '',
            country: '',
            from: '',
            to: '',
            details: '',

            rediractwidhSuccess: false,
            loading: false,
            error: '',
            successMassge:''
        }
    }

    componentDidMount(){
        // this.setState({workExpss:this.props.work})
    }

    onPracentClick = () => {
        this.setState({ error: '', loading: false })
        let toData = document.getElementById('to');
        let checked = document.getElementById('pracant').checked;
        if (checked === true) {
            toData.setAttribute('disabled', 'true');
            this.setState({ current: checked })
        } else if (checked === false) {
            toData.removeAttribute('disabled');
            this.setState({ current: checked })
        }
    }

    onWrokchangeValue = name => e => {
        // console.log('date => ',e.target.value);
        this.setState({ error: '', loading: false })
        this.setState({ [name]: e.target.value })
    }

    onWorkSubmit = () => {
        const { company, country, details, current, from, to, role,projectTitle,experianceType } = this.state;
        this.setState({ error: '', loading: true })
        const data = {
            projectTitle,
            experianceType,
            company,
            role,
            country,
            details,
            from,
            to,
            current,
        }
        addDetails(data, 'workrole').then(responce => {
            console.log('responce -> ', responce);
            if (responce.error) {
                this.setState({ error: responce.error.massage, loading: false })
            } else {
                let updateWork = this.state.workExpss;
                updateWork.unshift(data)
                this.setState({workExpss:updateWork, successMassge: responce.massage, loading: false })
                // window.location.reload()
                    document.getElementById('workForm').reset()
                    setTimeout(() => {
                        document.getElementById('work-modal-close').click()
                    }, 3000);
            }
            setTimeout(() => {
                this.setState({successMassge:'',error:''})
            }, 3000);

        })
    }
    onDeleteClicked = (id,index) => e => {
        // console.log('id -> ',id);
        if (window.confirm('Do you realy want to delete ?')) {
            deleteDetails('workrole', id).then(responce => {
                console.log('responce -> ', responce);
                if (responce.error) {
                    console.log('error -> ', responce.error);
                } else {
                    let updateWork = this.state.workExpss;
                    updateWork.splice(index,1)
                    this.setState({ workExpss:updateWork, error: '', loading: false })
                    // window.location.reload()
                    this.props.setmassage(responce.massage) // massage send to parent
                }
            })
            setTimeout(() => {
                this.setState({successMassge:'',error:''})
            }, 3000);
        }
    }

    showExpLeyout = () => {
        const { workExpss } = this.state
        return (
            <div className="pr-3 ps-5">
                {
                    workExpss.map((item, index) => {
                        return (
                            <div key={`${index}`} className='border-bottom py-2'>
                                <i className="font-s1p3 fas fa-building"></i>
                                <span className="fw-bold f-115rm ps-2">{item.company}
                                    {
                                        this.props.uid && isAuthenticate() && isAuthenticate().tokend === this.props.uid ? (
                                            <i onClick={this.onDeleteClicked(item._id)} title="Delete This" className="font-p9 iconT cursor-pointer ps-2 fas fa-trash"></i>
                                        ) : ''
                                    }
                                </span><br />
                                <div className='fs-6 ps-5'>
                                    <p className="">
                                        <span>

                                            <span className='fw-bold' >Position: </span>
                                            {item.role}
                                        </span>
                                        <span className="float-end">
                                            <span className='fw-bold' >Country: </span>
                                            {item.country}
                                        </span>
                                    </p>
                                    <p className="">
                                        <span>
                                            <span className='fw-bold'>From:</span>
                                            {new Date(item.from).toDateString()}
                                        </span>
                                        <span className="float-end">
                                            <sapn className='fw-bold'>To: </sapn>
                                            {item.current === true || item.to === null ? ('Pracent') : (<span>{new Date(item.to).toDateString()}</span>)}
                                        </span>
                                    </p>
                                    <div className='mt-2'>
                                        <b>Details:</b>
                                        <p className='ps-2'>
                                            {item.details}
                                        </p>
                                    </div>
                                </div>

                            </div>
                        )
                    })
                }
            </div>
        )
    }



    render() {
        const { error,successMassge, rediractwidhSuccess, loading } = this.state
        const userOrnot = this.props.uid && isAuthenticate() && isAuthenticate().tokend === this.props.uid;
        // if (rediractwidhSuccess) {
        //     return <Redirect to={`/profile/${this.props.userinfo.fname}-${this.props.userinfo.lname}-${this.props.userinfo.unqid}?masg=${error}`} />
        // }
        return (
            <div className='bg-white pb-3 p-3'>
                <h1 className='secund-heading border-bottom text-capitalize fs-4'>Work Experiance {this.props.uid && isAuthenticate() && isAuthenticate().tokend === this.props.uid ? <i className="editIcon fas fa-plus" data-bs-toggle="modal" data-bs-target="#addExperiance"></i> : ''}</h1>
                <div className=''>
                    {this.props.work && this.props.work[0] && this.props.work[0].company ? (
                        <div>
                            {this.showExpLeyout()}
                        </div>
                    ) : (
                            <div>
                                {userOrnot ? 'Pleas add your Work experiance' : 'Freshar'}
                            </div>
                        )}
                </div>


                {/* <!-- Modal --> */}
                <div className="modal fade z9999" id="addExperiance" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div className="modal-dialog z9999">
                        <div className="modal-content z9999">
                            <div className="modal-header">
                                <h5 className="modal-title secund-heading text-capitalize fs-4" id="staticBackdropLabel">Add Experiance</h5>
                                <button id="work-modal-close" type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body">
                                <div className="text-center">
                                    {
                                        loading ? (
                                            <div className='d-flex'>
                                                <div className=' m-auto'>
                                                    <div className="spinner-border text-danger" role="status">
                                                        <span className="visually-hidden">Loading...</span>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : ''
                                    }
                                </div>
                                {
                                    error ? (
                                        <div className="alert alert-danger alert-dismissible text-start fade show" role="alert">
                                            <strong>Error!</strong> {error}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                {
                                    successMassge ? (
                                        <div className="alert alert-success alert-dismissible text-start fade show" role="alert">
                                            <strong>Success!</strong> {successMassge}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                <form className="row g-3" id="workForm">
                                <div className="col-12">
                                        <label for="inputState" className="form-label required">Type</label>
                                        <small>(select which type of experience eg. job, internship or project etc.)</small>
                                        <select id="inputState" className="form-select" onChange={this.onWrokchangeValue('experianceType')}>
                                            <option disabled selected hidden >What type of experience is thare ?</option>


                                            <option value="Job">Job</option>
                                            <option value="Internship">Internship</option>
                                            <option value="Traning Course">Traning Course</option>
                                            <option value="Personal Project">Personal Project</option>
                                        </select>
                                    </div>

                                    <div className="col-12">
                                        <label htmlFor="company" className="form-label required">company Name :</label>
                                        <input type="text" onChange={this.onWrokchangeValue('company')} className="form-control" id="company" name='company' placeholder="ex. abs pvt. ltd." />
                                    </div>
                                    <div className="col-md-6">
                                        <label htmlFor="role" className="form-label required">Position :</label>
                                        <input type="text" onChange={this.onWrokchangeValue('role')} className="form-control" name="role" id="role" placeholder="ex. Softwere engineer" />
                                    </div>
                                    <div className="col-md-6">
                                        <label htmlFor="wCountry" className="form-label required">Country :</label>
                                        <input type="text" onChange={this.onWrokchangeValue('country')} className="form-control" name="wCountry" id="wCountry" placeholder='ex. USA' />
                                    </div>
                                    <div className="col-md-4">
                                        <label htmlFor="from" className="form-label required">From :</label>
                                        <input type="date" onChange={this.onWrokchangeValue('from')} className="form-control" name="from" id="from" />
                                    </div>
                                    <div className="col-md-4">
                                        <label htmlFor="to" className="form-label">To :</label>
                                        <input type="date" onChange={this.onWrokchangeValue('to')} className="form-control" name="to" id="to" />
                                    </div>
                                    <div className="col-4 mt-auto">
                                        <div className="form-check">
                                            <input onChange={this.onPracentClick} className="form-check-input" type="checkbox" id="pracant" />
                                            <label className="form-check-label" htmlFor="pracant">

                                                Pracant
                                            </label>
                                        </div>
                                    </div>
                                    <div className="col-12">
                                        <label htmlFor="details" className="form-label">Details :</label>
                                        <textarea onChange={this.onWrokchangeValue('details')} maxLength='250' minLength='20' rows='5' type="text" className="form-control" name="details" id="details" />
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button onClick={this.onWorkSubmit} type="button" className="btn full-primary">Add</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default WorkExpss;
