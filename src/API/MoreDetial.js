/*
    @Description => hare all creat and delete -> workrole, skill nand education
    @rootapi => http://localhost:8080/api/moreDetail
    @method => Post and DELET
    @access => Privet (token require)
*/

import axios from "axios";
import { APIurl } from "../variables";
import { isAuthenticate } from "./Autho";

const baseUrl = `${APIurl}/moreDetail`
const wtToken = isAuthenticate().jwtoken

// creat Details 
export const addDetails = (data,EducationOrWorkOrSkills) => {
    return axios({
        url:`/${EducationOrWorkOrSkills}`,
        baseURL:baseUrl,
        method:"POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${wtToken}`
        },
        data: data
    }).then(responce => responce.data)
    .catch(error => {
        let errorResSjson;
        let errorjson;
        if (error.responce) {
            console.log('error.responce -> ', error.responce)
        }
        else if (error.request) {
            errorResSjson = error.request.response  // '{"key":"value"}' sring type json
            errorjson = JSON.parse(errorResSjson)
        }
        else {
            console.log('error.message -> ', error.message)
        }
        return { error: errorjson }
    });
}

// delete Details 
export const deleteDetails = (EducationOrWorkOrSkills,id) => {
    return axios({
        url:`${EducationOrWorkOrSkills}/${id}`,
        baseURL:baseUrl,
        method:"DELETE",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${wtToken}`
        }
    }).then(responce => responce.data)
    .catch(error => {
        // console.log(error);
        let errorResSjson;
        let errorjson;
        if (error.responce) {
            console.log('error.responce -> ', error.responce)
        }
        else if (error.request) {
            errorResSjson = error.request.response  // '{"key":"value"}' sring type json
            errorjson = JSON.parse(errorResSjson)
        }
        else {
            console.log('error.message -> ', error.message)
        }
        return { error: errorjson }
    });
}