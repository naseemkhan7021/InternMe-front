import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { isAuthenticate, logout } from '../API/Autho';
import '../utilities.css';
import Logo from './logo';
import { Backendurl,imgurl } from '../variables';
import './navigation.css';

class Navigation extends Component {
    constructor(params) {
        super(params)
        this.state={
            redirect:false,
            userType:''
        }
    }    
    clickLogOut = () => {
        logout().then(data => {
            console.log('data -> ', data);
            if (data.success) {
                window.location.reload()
            }
        })
    }
    clickUserType = e => {
        console.log('value -> ',e.target.value);
        document.getElementById('modale-close-btn').click()
        this.setState({userType:e.target.value,redirect:true})
    }
    componentDidMount() {
        // console.log('p logdin => ',this.props.logdin);
        // console.log('header info -> ',this.props.headerinfo);
        console.log('img info -> ', this.props.profileimg);
    }

    render() {
        const {redirect,userType} = this.state
        if(redirect){
            return (<Redirect to={{pathname:'/login',state:{userType}}} />)
        }
        return (
            <>
                {
                    this.props.logdin ? (
                        // (console.log('this is -> ',))
                        <div className="position-fixed navController">

                            <nav className='d-flex justify-content-between justify-content-between'>
                                <div className='navleft d-flex align-items-center g-3'>
                                    <Link to=''><b className='symbol symbofeed'>me</b></Link>
                                    <div className='searchHead ms-2'>
                                        <div className='searchsubHead'>
                                            <input type='search' placeholder='Search' id='search' name='search' className='form-control search' />
                                            <span className='searchicon'><i className="fas fa-search"></i></span>
                                        </div>
                                    </div>
                                </div>

                                <div className='navright'>
                                    <ul className='m-auto'>
                                        <li>

                                            <Link to='/feed'>
                                                <i className="fas fa-home"></i><span>Home</span></Link>
                                        </li>
                                        {/* <li><Link to=''>HotJob</Link></li> */}

                                        <li className="nav-item dropdown">

                                            <a className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                <i className="fas fa-briefcase"></i>
                                                <span>HotJob</span>
                                            </a>
                                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                                <li><Link to='/job' className="dropdown-item" >Internship</Link></li>
                                                <li><Link to='/job' className="dropdown-item" >Intermidiat</Link></li>
                                                <li><Link to='/job' className="dropdown-item" >Associate</Link></li>
                                                <li><hr className="dropdown-divider" /></li>
                                                <li><Link to='/job' className="dropdown-item" >Professional</Link></li>
                                                <li><Link to='/job' className="dropdown-item" >Export</Link></li>
                                            </ul>
                                        </li>

                                        <li><Link to='/people'>
                                            <i className="fas fa-users"></i><span>Find Connecitons</span></Link></li>

                                        <li>
                                            <Link to='#' className="nav-link dropdown-toggle" id="navbarDropdown2" role="button" data-bs-toggle="dropdown" aria-expanded="false">

                                                <img src={`${imgurl}/${this.props.profileimg}`}
                                                    onError={i => i.currentTarget.src = `${process.env.PUBLIC_URL}/defaultuser.jpg`} className='rounded-circle' width='30' height='30' style={{ objectFit: 'cover' }} /><span>profile</span></Link>
                                            {/* defoult testing  */}
                                            {/* <img src={`${process.env.PUBLIC_URL}/defaultuser.jpg`} width='30' className='rounded-circle' /><span>profile</span></Link> */}

                                            <ul className="dropdown-menu" aria-labelledby="navbarDropdown2">
                                                <li><Link to={`/profile/${this.props.fname}-${this.props.lname}-${this.props.unid}`} className="dropdown-item" >Show My Profile</Link></li>

                                                <li><Link to='#' onClick={this.clickLogOut} className="dropdown-item" >Logout</Link></li>

                                            </ul>
                                        </li>

                                    </ul>
                                </div>
                            </nav>
                        </div>
                    )
                        :
                        (
                            <>
                            <nav className='container'>


                                <div id='header' className="d-flex justify-content-between align-items-center">
                                    <div id="LogoandSearch">
                                        <Logo />
                                        {/* this is importent another time  */}
                                        {/* <div className='search'>
                                        <form action='' method='Post'>
                                            <input type='text' name='search' id='search' placeholder='find any thing' />
                                        </form>
                                    </div> */}
                                    </div>
                                    <div id='Singinandsingup' className='d-flex'>
                                        {/* <Link to={'/singup'} className="btn fw-bold sinup-btn">Join now</Link> */}
                                        <Link to={'/login'} className="btn fw-bold btn-outline login-btn home-login-btn-mp" data-bs-toggle="modal" data-bs-target="#staticBackdrop">Login</Link>
                                        <span className="border-end"></span>
                                        <p className="nav-link dropdown-toggle" id="singupDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"> Join now
                                        </p>
                                        <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                                <li><Link to='/singup/student' className="dropdown-item" >as a Student</Link></li>
                                                <li><Link to='/singup/employer' className="dropdown-item" >as a Employer</Link></li>
                                            </ul>
                                        
                                    </div>
                                </div>
                            </nav>
                                {/* <!-- Button trigger modal --> */}
                                {/* <button type="button" className="btn btn-primary" >
                                  Launch static backdrop modal
                                </button> */}

                                {/* <!-- Modal --> */}
                                <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                  <div className="modal-dialog modal-dialog-centered">
                                    <div className="modal-content">
                                      <div className="modal-header">
                                        <h5 className="modal-title secund-heading fw-bold" id="staticBackdropLabel">Login as a</h5>
                                        <button id='modale-close-btn' type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                      </div>
                                      <div className="modal-body">
                                        {/* <Login/> */}
                                        <div className="text-center row g-3">
                                            <div>
                                                <button onClick={this.clickUserType} type='button' className="btn w-75 button-outline login-btn" value='employer'> as a Employer</button>
                                            </div>
                                            <div>
                                            <button onClick={this.clickUserType} type='button' className="btn w-75 button-outline login-btn" value='student'>as a Student</button>
                                            </div>
                                        </div>
                                      </div>
                                      <div className=" modal-footer m-auto">
                                      <h6 className='my-3'><span className='fw-normal'>New to InternMe? </span>Join now (<Link className='a-deco-non a-primary' to={'/singup/student'}>Student</Link>/<Link className='a-deco-non a-primary' to={'/singup/employer'}>Employer</Link>)</h6>

                                      </div>
                                      
                                    </div>
                                  </div>
                                </div>
                            </>
                        )
                }


            </>
        );
    }
}

export default Navigation;
