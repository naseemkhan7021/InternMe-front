import { APIurl } from '../variables';
import { isAuthenticate } from './Autho';
import axios from 'axios';

// console.log(`${url}/singup`);
// const wtToken = isAuthenticate().jwtoken;
// console.log('tken -> ',wtToken);
export const creat_profileOrpost = (urproOrhrproOrurpostOrhrpost,profilidOrpostId,profileTookeOrpostToke,data) => {
    // console.log('data : -> ',data);
    let url = `/${urproOrhrproOrurpostOrhrpost}/new-info/${profilidOrpostId}-${profileTookeOrpostToke}`;
    // console.log('data -> ',data);
    // console.log('url -> ',url);
    return axios({
        url, 
        baseURL: APIurl,
        method: 'POST',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        data: data
    })
        .then(responce => responce)
        .catch(error => {
            // console.log(error);
            let errorResSjson;
            let errorjson;
            // console.log('error line 75 -> ', error);
            if (error.responce) {
                console.log('error.responce -> ', error.responce)
            }
            else if (error.request) {
                // console.log('error.request -> ', error.request)
                errorResSjson = error.request.response  // '{"key":"value"}' sring type json
                errorjson = JSON.parse(errorResSjson)
            }
            else {
                console.log('error.message -> ', error.message)
            }
            return { error: errorjson.error }
        });
}

export const update_profileOrpost = (urproOrhrproOrurpostOrhrpost,profilidOrpostId,profileTookeOrpostToke,data) => {
    // console.log('data : -> ',data);
    let wtToken = isAuthenticate().jwtoken;

    let url = `/${urproOrhrproOrurpostOrhrpost}/update-info/${profilidOrpostId}-${profileTookeOrpostToke}`;
    // console.log('jwtoken -> ',wtToken);
    // console.log('url -> ',url);
    return axios({
        url, 
        baseURL: APIurl,
        method: 'PUT',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${wtToken}`
        },
        data: data
    })
        .then(responce => responce)
        .catch(error => {
            // console.log(error);
            let errorResSjson;
            let errorjson;
            // console.log('error line 75 -> ', error);
            if (error.responce) {
                console.log('error.responce -> ', error.responce)
            }
            else if (error.request) {
                // console.log('error.request -> ', error.request)
                errorResSjson = error.request.response  // '{"key":"value"}' sring type json
                errorjson = JSON.parse(errorResSjson)
            }
            else {
                console.log('error.message -> ', error.message)
            }
            return { error: errorjson.error }
        });
}

export const checkUser = async (id,token) => {
    return await axios({
        url:`/userProfile/user-check/${id}-${token}`,
        baseURL:APIurl,
        method:'GET',
        headers:{
            "Accept": "application/json",
            "Content-Type": "application/json",
        }
    }).then(responce => responce.data)
    .catch(error => {
        // console.log(error);
        let errorResSjson;
        let errorjson;
        // console.log('error line 75 -> ', error);
        if (error.responce) {
            console.log('error.responce -> ', error.responce)
        }
        else if (error.request) {
            // console.log('error.request -> ', error.request)
            errorResSjson = error.request.response  // '{"key":"value"}' sring type json
            errorjson = JSON.parse(errorResSjson)
        }
        else {
            console.log('error.message -> ', error.message)
        }
        return { error: errorjson.error }
    });
}

// by id only 
export const showSingluser = (id) => {
    return axios({
        url:`/userProfile/user/${id}`,
        baseURL:APIurl,
        method:'GET',
        headers:{
            "Accept":"application/json",
            "Content-Type":"application/json"
        }
    }).then(responce => responce.data)
    .catch(error => {
        // console.log(error);
        let errorResSjson;
        let errorjson;
        // console.log('error line 75 -> ', error);
        if (error.responce) {
            console.log('error.responce -> ', error.responce)
        }
        else if (error.request) {
            // console.log('error.request -> ', error.request)
            errorResSjson = error.request.response  // '{"key":"value"}' sring type json
            errorjson = JSON.parse(errorResSjson)
        }
        else {
            console.log('error.message -> ', error.message)
        }
        return { error: errorjson.error }
    });
}

// by id only 
export const showSingluserbyName = ({fname,lname,uid}) => {
    // console.log('fname lname id ',fname ,' ',lname,' ',uid)
    return axios({
        url:`/userProfile/userbyname/${fname}-${lname}-${uid}`,
        baseURL:APIurl,
        method:'GET',
        headers:{
            "Accept":"application/json",
            "Content-Type":"application/json"
        }
    }).then(responce => responce.data)
    .catch(error => {
        // console.log(error);
        let errorResSjson;
        let errorjson;
        // console.log('error line 75 -> ', error);
        if (error.responce) {
            console.log('error.responce -> ', error.responce)
        }
        else if (error.request) {
            // console.log('error.request -> ', error.request)
            errorResSjson = error.request.response  // '{"key":"value"}' sring type json
            errorjson = JSON.parse(errorResSjson)
        }
        else {
            console.log('error.message -> ', error.message)
        }
        return { error: errorjson.error }
    });
}
