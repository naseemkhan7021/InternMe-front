import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { isAuthenticate } from '../../API/Autho';
import { addDetails, deleteDetails } from '../../API/MoreDetial';

class EducationsNskills extends Component {
    constructor(params) {
        super(params)
        this.state = {
            skills: this.props.skills,
            educations: this.props.education,

            // for eductions
            school: '',
            fieldofstudy: '',
            degree: '',
            start: '',
            end: '',
            grade: '',
            description: '',
            status: '',

            // for skills 
            name: '', // name is skill name (python or web developer or any)
            level: '',

            // for uses 
            error: '',
            successMassge: '',
            loading: false,
            rediractwidhSuccess: false
        }
    }

    hadelValueChange = name => e => {
        this.setState({ error: '', loading: false })
        this.setState({ [name]: e.target.value })
    }

    onSubmitClick = (e) => {
        e.preventDefault()
        this.setState({ error: '', loading: true })
        // let Edbtn = document.getElementById('resetEd') // educations reset bt
        // let Slbtn = document.getElementById('resetSl') // skill reset btn
        const { name, level, school, degree, fieldofstudy, start, end, grade, description, status } = this.state;
        const eduData = {
            school, degree, fieldofstudy, start,
            end, grade, description, status
        }
        const skillData = { name, level }
        switch (e.target.id) {
            case 'edu':
                // console.log('this is edu -> ', eduData);
                addDetails(eduData, 'education').then(responce => {
                    console.log('responce -> ', responce);
                    if (responce.error) {
                        this.setState({ error: responce.error.massage, loading: false })
                    }
                    else {
                        let updateducations = this.state.educations
                        updateducations.unshift(eduData)
                        this.setState({ educations: updateducations, error: '', successMassge: responce.massage, loading: false })
                        // window.location.reload()
                        this.props.setmassage(responce.massage) // massage send to parent
                        document.getElementById('resetEd').reset()

                    }
                    setTimeout(() => {
                        document.getElementById('edu-model-close').click()
                    }, 3000);
                })
                break;
            case 'skill':
                addDetails(skillData, 'skill').then(responce => {
                    console.log('responce -> ', responce);
                    if (responce.error) {
                        this.setState({ error: responce.error.massage, loading: false })
                    }
                    else {
                        let updatedskills = this.state.skills
                        updatedskills.unshift(skillData)
                        this.setState({ skills: updatedskills, error: '', successMassge: responce.massage, loading: false })
                        // window.location.reload()
                        document.getElementById('resetSl').reset()
                        this.props.setmassage(responce.massage) // massage send to parent
                        setTimeout(() => {
                            document.getElementById('skill-model-close').click()
                        }, 3000);
                    }
                })

                break;
            default:
                break;

        }

        setTimeout(() => {
            this.setState({ successMassge: '', error: '' })
        }, 3000);

    }

    onDeleteClicked = (id, index) => e => {
        // console.log('id -> ',id);
        if (window.confirm('Do you realy want to delete ?')) {
            switch (e.target.id) {
                case 'skill_icon':
                    deleteDetails('skill', id).then(responce => {
                        console.log('responce -> ', responce);
                        if (responce.error) {
                            console.log('error -> ', responce.error);
                        } else {
                            let updatedskills = this.state.skills
                            updatedskills.splice(index, 1)
                            this.setState({ skills: updatedskills, loading: false })
                            this.props.setmassage(responce.massage) // massage send to parent

                        }
                    })
                    break;
                case 'edu_icon':
                    deleteDetails('education', id).then(responce => {
                        console.log('responce -> ', responce);
                        if (responce.error) {
                            console.log('error -> ', responce.error);
                        } else {
                            let updateducations = this.state.educations
                            updateducations.splice(index, 1)
                            this.setState({ educations: updateducations, loading: false })
                            this.props.setmassage(responce.massage) // massage send to parent
                        }
                    })
                    break;

                default:
                    break;
            }
        }
    }

    showSkillsLeyout = () => {
        const { skills } = this.state;
        return (
            <div className='d-flex flex-wrap col g-3 ps-5 pr-3'>
                {
                    skills.map((item, index) => {
                        return (
                            <div key={`${item.name}-${index}`} className="ps-5 pb-3">
                                <p className="fw-bold f-115rm">{item.name}
                                    {
                                        this.props.uid && isAuthenticate() && isAuthenticate().tokend === this.props.uid ? (
                                            <i id="skill_icon" onClick={this.onDeleteClicked(item._id, index)} title="Delete This" className="font-p9 iconT cursor-pointer ps-2 fas fa-trash"></i>
                                        ) : ''
                                    }
                                </p>
                                <p className="">{item.level}</p>
                            </div>
                        )
                    })
                }
            </div>
        )
    }

    showEducationsLeyout = () => {
        const { educations } = this.state;
        return (
            <div className="pr-3 ps-5">
                {
                    educations.map((item, index) => {
                        return (
                            <div key={`${index}`} className='border-bottom py-2'>
                                <i className="font-s1p3 fas fa-graduation-cap"></i>
                                <span className="fw-bold f-115rm ps-2">{item.school}
                                    {
                                        this.props.uid && isAuthenticate() && isAuthenticate().tokend === this.props.uid ? (
                                            <i id="edu_icon" onClick={this.onDeleteClicked(item._id, index)} title="Delete This" className="font-p9 iconT cursor-pointer ps-2 fas fa-trash"></i>
                                        ) : ''
                                    }
                                </span><br />
                                <div className='fs-6 ps-5'>
                                    <p className="">
                                        <span>

                                            <span className='fw-bold' >Field of Study: </span>
                                            {item.fieldofstudy}
                                        </span>
                                        <span className="float-end">
                                            <span className='fw-bold' >Degree: </span>
                                            {item.degree}
                                        </span>
                                    </p>
                                    <p className="">
                                        <span>
                                            <span className='fw-bold'>From:</span>
                                            {new Date(item.start).toDateString()}
                                        </span>
                                        <span className="float-end">
                                            <sapn className='fw-bold'>To: </sapn>
                                            {new Date(item.end).toDateString()}
                                        </span>
                                    </p>
                                    <div className='mt-2'>
                                        <b>Description:</b>
                                        <p className='ps-2'>
                                            {item.description}
                                        </p>
                                    </div>
                                </div>

                            </div>
                        )
                    })
                }
            </div>
        )
    }
    componentDidMount() {
        // console.log('id -> ',this.props.uid);
        // this.setState({ skills: this.props.skills, educations: this.props.education })
    }
    render() {
        const { error, loading, successMassge, educations } = this.state;
        const userOrnot = this.props.uid && isAuthenticate() && isAuthenticate().tokend === this.props.uid;
        // if (rediractwidhSuccess) {
        //     return <Redirect to={`/profile/${this.props.userinfo.fname}-${this.props.userinfo.lname}-${this.props.userinfo.unqid}?masg=${error}`} />
        // }
        return (
            <div>
                <div className='bg-white br-1rem p-3 mb-3'>
                    <h1 className='secund-heading border-bottom text-capitalize fs-4'>Educations
                    {this.props.uid && isAuthenticate() && isAuthenticate().tokend === this.props.uid ? <i className="editIcon fas fa-plus" data-bs-toggle="modal" data-bs-target="#addEducation"></i>
                            :
                            ''}</h1>
                    <div classNamep='p-3'>

                        {this.props.education && this.props.education[0] && this.props.education[0].school ? (<div>{this.showEducationsLeyout()}</div>)
                            :
                            (<div>{userOrnot ? 'Pleas add your Educations details'
                                :
                                'No Education added by this user'}</div>)}
                    </div>
                </div>
                <div className='bg-white br-1rem p-3'>
                    <h1 className='secund-heading border-bottom text-capitalize fs-4'><i className="fas fa-brain"></i><span className="ps-2">Skills</span>{this.props.uid && isAuthenticate() && isAuthenticate().tokend === this.props.uid ? <i className="editIcon fas fa-plus" data-bs-toggle="modal" data-bs-target="#addSkill"></i>
                        :
                        ''}</h1>
                    <div classNamep='p-3'>

                        {this.props.skills && this.props.skills[0] && this.props.skills[0].level ? (<div>{this.showSkillsLeyout()}</div>)
                            :
                            (<div>{userOrnot ? 'Pleas add your Skills details' : 'No Skills added by this user'}</div>)}
                    </div>
                </div>

                {/* education model  */}

                {/* <!-- Modal --> */}
                <div className="modal fade z9999" id="addEducation" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div className="modal-dialog z9999">
                        <div className="modal-content z9999">
                            <div className="modal-header">
                                <h5 className="modal-title secund-heading text-capitalize fs-4'" id="staticBackdropLabel">Add Education Details</h5>
                                <button id="edu-model-close" type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body">
                                <div className="text-center">
                                    {
                                        loading ? (
                                            <div className='d-flex'>
                                                <div className=' m-auto'>
                                                    <div className="spinner-border text-danger" role="status">
                                                        <span className="visually-hidden">Loading...</span>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : ''
                                    }
                                </div>
                                {
                                    error ? (
                                        <div className="alert alert-danger alert-dismissible text-start fade show" role="alert">
                                            <strong>Error!</strong> {error}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                {
                                    successMassge ? (
                                        <div className="alert alert-success alert-dismissible text-start fade show" role="alert">
                                            <strong>Success!</strong> {successMassge}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                <form className="row g-3" id='resetEd'>

                                    <div className='col-12'>
                                        <div className="form-check form-check-inline">
                                            <input onChange={this.hadelValueChange('status')} className="form-check-input" type="radio" id="eduPursuing" name='eduStatus' value="Pursuing" />
                                            <label className="form-check-label" for="eduPursuing">Pursuing</label>
                                        </div>
                                        <div className="form-check form-check-inline">
                                            <input onChange={this.hadelValueChange('status')} className="form-check-input" type="radio" id="eduCompleted" name='eduStatus' value="Complited" />
                                            <label className="form-check-label" for="eduCompleted">Copleted</label>
                                        </div>
                                    </div>

                                    <div className="col-12">
                                        <label for="inputState" className="form-label required">Title</label>
                                        <select id="inputState" className="form-select" onChange={this.hadelValueChange('degree')}>
                                            <option disabled selected hidden value='secudnry schoole'>How far have you read</option>

                                            {/* now if not my map loop only for secundry and higher secundry (padding)*/}
                                            {educations && educations.map((item, i) =>
                                            (
                                                <>
                                                    {/* {console.log(item.degree === 'secudnry schoole')} */}
                                                    {
                                                        item.degree === 'secudnry schoole' ? (<option value='secudnry schoole'>secudnry schoole</option>) : ''
                                                    }
                                                </>
                                            )
                                            )}
                                            {educations && educations.map((item, i) =>
                                            (
                                                <>
                                                    {
                                                        item.degree === 'Higher Secondry School' ? '' : (<option value='Higher Secondry School'>Higher Secondry School</option>)
                                                    }
                                                </>
                                            )
                                            )}

                                            <option value="Diploma">Diploma</option>
                                            <option value="Bachelor Degree">Bachelor Degree</option>
                                            <option value="Master Degree">Master Degree</option>
                                            <option value="PhD">PhD</option>
                                        </select>
                                    </div>
                                    <div className="col-7">
                                        <label for="schoolname" className="form-label">School/College Name :</label>
                                        <input onChange={this.hadelValueChange('school')} type="text" className="form-control" id="schoolname" name='schoolname' placeholder="ex. abs pvt. ltd." />
                                    </div>
                                    <div className="col-md-5">
                                        <label for="field" className="form-label">Field of Study :</label>
                                        <input onChange={this.hadelValueChange('fieldofstudy')} type="text" className="form-control" name="field" id="field" placeholder="ex. IT" />
                                    </div>

                                    <div className="col-md-5">
                                        <label for="start" className="form-label">Start :</label>
                                        <input onChange={this.hadelValueChange('start')} type="date" className="form-control" name="start" id="start" />
                                    </div>
                                    <div className="col-md-5">
                                        <label for="end" className="form-label">End :</label>
                                        <input onChange={this.hadelValueChange('end')} type="date" className="form-control" name="end" id="end" />
                                    </div>
                                    <div className="col-md-2">
                                        <label for="grade" className="form-label">Grade :</label>
                                        <input onChange={this.hadelValueChange('grade')} type="text" maxLength='4' className="form-control" name="grade" id="grade" />
                                    </div>

                                    <div className="col-12">
                                        <label for="discription" className="form-label">Description :</label>
                                        <textarea onChange={this.hadelValueChange('description')} maxLength='250' minLength='20' rows='5' type="text" className="form-control" name="discription" id="discription" />
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button onClick={this.onSubmitClick} type="submit" className="btn full-primary" id="edu">Add</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/* skills model */}

                {/* <!-- Button trigger modal --> */}
                {/* <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                    Launch static backdrop modal
                </button> */}

                {/* <!-- Modal --> */}
                <div className="modal fade z9999" id="addSkill" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div className="modal-dialog z9999">
                        <div className="modal-content z9999">
                            <div className="modal-header">
                                <h5 className="modal-title secund-heading text-capitalize fs-4'" id="staticBackdropLabel">Add Skills</h5>
                                <button id="skill-model-close" type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body ">
                                <div className="text-center">
                                    {
                                        loading ? (
                                            <div className='d-flex'>
                                                <div className=' m-auto'>
                                                    <div className="spinner-border text-danger" role="status">
                                                        <span className="visually-hidden">Loading...</span>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : ''
                                    }
                                </div>
                                {
                                    error ? (
                                        <div className="alert alert-danger alert-dismissible text-start fade show" role="alert">
                                            <strong>Error!</strong> {error}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                {
                                    successMassge ? (
                                        <div className="alert alert-success alert-dismissible text-start fade show" role="alert">
                                            <strong>Success!</strong> {successMassge}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>
                                    ) : ''

                                }
                                <form className="row g-3" id='resetSl'>
                                    <div className="col-12">
                                        <label for="skill" className="form-label">Skill :</label>
                                        <input onChange={this.hadelValueChange('name')} type="text" maxLength='50' className="form-control" id="skill" name='skill' placeholder="ex. Python Developer." />
                                        <small className='danger'>Pleas fill one skill at a time</small>
                                    </div>
                                    <div className="col-12">
                                        <label className="form-label">Level :</label>
                                        <div className="form-check">
                                            <input onChange={this.hadelValueChange('level')} className="form-check-input" id="biginner" type="radio" name='level' value="Beginner" />
                                            <label className="form-check-label" htmlFor="biginner">
                                                Beginner
                                            </label>
                                        </div>
                                        <div className="form-check">
                                            <input onChange={this.hadelValueChange('level')} className="form-check-input" id="intermediate" type="radio" name='level' value="Intermediate" /><label className="form-check-label" htmlFor="intermediate">
                                                Intermediate
                                            </label>
                                        </div>
                                        <div className="form-check">
                                            <input onChange={this.hadelValueChange('level')} className="form-check-input" id="advance" type="radio" name='level' value="Advance" /><label className="form-check-label" htmlFor="advance">
                                                Advance
                                            </label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button onClick={this.onSubmitClick} type="submit" className="btn full-primary" id="skill">Add</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EducationsNskills;
