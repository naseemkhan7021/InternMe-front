import React, { Component } from 'react';
import { checkUser, showSingluser, showSingluserbyName } from './API/Profile';
import Navigation from './core/Navigation';
import NotFound404 from './NotFound404';
import Intro from './view/profile/Intro';
import Recommend from './view/profile/Recommend';
// css file 
import './utilities.css';
import './view/profile/profile.css'
import Contect from './view/profile/Contect';
import EducationsNskills from './view/profile/EducationsNskills';
import WorkExpss from './view/profile/WorkExpss';
import { docurl } from './variables';
import { Document, Page } from 'react-pdf';
import Dashboard from './view/profile/Dashboard';
import { isAuthenticate } from './API/Autho';
import querystring from 'querystring';


class Profile extends Component {
    constructor(params) {
        super(params)
        this.state = {
            loading: false,
            logdin: false,
            notFound: false,
            data: {},
            headData: {},
            headImgpic: '',
            imgpic: '',
            profileCover: '',

            error: '',
            massage: ''
        }
    }

    setmassage = (massage) => {
        this.setState({ massage: massage })

        setTimeout(() => {
            this.setState({ massage: '' })
        }, 6000);
    }

    componentDidMount() {
        this.setState({ loading: true })
        if (isAuthenticate()) {
            let id = isAuthenticate().tokend;
            let token = isAuthenticate().token;
            checkUser(id, token).then(responce => {
                // console.log('reponce for isAuth -> ',responce)
                if (responce.error) {
                    // console.log('erro -> ', responce.error);
                    this.setState({ logdin: false })
                    return false
                }
                else if (responce.success || responce.detail) {
                    // console.log('inof ',parsinfo );
                    // console.log('respoce -> ', responce);
                    this.setState({ logdin: true })
                    showSingluser(id).then(responce => {
                        // console.log('singl user responce ', responce);
                        this.setState({ headData: responce.user, headImgpic: responce.user.profileImg.fileName })
                    })
                    // return responce;
                }
            })
        }


        let data = {
            fname: this.props.match.params.fname,
            lname: this.props.match.params.lname,
            uid: this.props.match.params.uid
        }
        showSingluserbyName(data).then(responce => {
            // console.log('responce pro -> ',responce);
            if (responce.error) {
                this.setState({ notFound: true, loading: false })
            } else {
                this.setState({ data: responce.user, profileCover: responce.user.profileCoverimg.fileName, imgpic: responce.user.profileImg.fileName, email: responce.user.user.email, loading: false })
                document.title = `${responce.user.firstName} ${responce.user.lastName} | Profile`
            }
        })
    }
    render() {
        const { error, massage, loading, notFound, email, data, imgpic, logdin, profileCover, headData, headImgpic } = this.state;
        if (notFound) {
            return <NotFound404 usernotfound={true} />
        }
        return (
            loading ? (
                <div className='min-vw-100 min-vh-100 d-flex'>
                    <div className=' m-auto'>

                        <div className="spinner-border text-danger" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            ) : (
                <div>
                    <header>
                        <Navigation fname={headData.firstName} lname={headData.lastName} unid={headData.uniq} profileimg={headImgpic} logdin={logdin} />
                    </header>
                    <div className='content-body-padding'>
                        <section className='container'>

                            <div className="row g-4 pb-3">
                                <div className="col-md-9">
                                    {
                                        error ? (<div className="alert alert-danger alert-dismissible text-start fade show" role="alert">
                                            <strong>Error!</strong> {error}.
                                            {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                        </div>) : ''
                                    }
                                    {
                                        massage ? (
                                            <div className="alert allert-top-center-position alert-success alert-dismissible text-start fade show" role="alert">
                                                <strong>Success!</strong> {massage}.
                                                {/* <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" /> */}
                                            </div>
                                        ) : ''
                                    }
                                    <div id='intro-sections' className='eachSection'>
                                        {/* this is intro sections img coverimg about join connections  */}
                                        <Intro setmassage={this.setmassage} unqid={data.uniq} uid={data.user && data.user._id} userintro={{ imgpic: imgpic, imgpicpath: data.profileImg && data.profileImg.Path, coverimg: profileCover, coverimgpath: data.profileCoverimg && data.profileCoverimg.Path, name: data.userName, docname: data.cvResume && data.cvResume.fileName, docpath: data.cvResume && data.cvResume.Path, fname: data.firstName, lname: data.lastName, gander: data.gander, about: data.about, network: data.connections, join: data.createdAt }} />
                                    </div>

                                    {
                                        data.user && isAuthenticate() && isAuthenticate().tokend === data.user._id ? (
                                            <div id='eduNsk-section' className='eachSection'>
                                                <Dashboard />
                                            </div>
                                        ) : ''
                                    }


                                    <div id='eduNsk-section' className='eachSection'>
                                        {/* hare is all Education details   */}
                                        <EducationsNskills setmassage={this.setmassage} userinfo={{ fname: data.firstName, lname: data.lastName, unqid: data.uniq }} education={data.educations} skills={data.skills} uid={data.user && data.user._id} />
                                    </div>
                                    <div id='work-section' className='eachSection'>
                                        {/* hare is all Education details   */}
                                        <WorkExpss setmassage={this.setmassage} userinfo={{ fname: data.firstName, lname: data.lastName, unqid: data.uniq }} work={data.workrole} uid={data.user && data.user._id} />
                                    </div>

                                    <div id='contect-section' className='eachSection'>
                                        {/* hare is all contect details   */}
                                        <Contect setmassage={this.setmassage} userinfo={{ fname: data.firstName, lname: data.lastName, unqid: data.uniq }} uid={data.user && data.user._id} userContect={{ phone: data.phone, address: data.address, social: data.social, email: email }} />
                                    </div>
                                    <div id='contect-section' className='eachSection'>
                                        <div className='bg-white pb-3 p-3'>
                                            <h1 className='secund-heading text-capitalize fs-4'>Resume</h1>
                                            {/* <div className='overflow-hidden'>
                                                 <Document file={data.cvResume &&  `${docurl}/${data.cvResume.fileName}`}></Document>
                                                </div> */}
                                            {data.cvResume && data.cvResume.fileName ?
                                                (<a target="_blanck" href={data.cvResume && `${docurl}/${data.cvResume.fileName}`}> Show My Resume</a>) : (
                                                    <div>No Document upload by <b>{data.userName}</b></div>
                                                )
                                            }
                                            {data.workSample && data.workSample ? (
                                                <>
                                                    <h1 className='m-0 mt-2 secund-heading text-capitalize fs-4 border-top'>Hare is my work sample</h1>
                                                    <small>(Go thare show my work)</small>
                                                    <br />
                                                    <a target="_blanck" href={data.workSample && `${data.workSample}`}> Go to My Work</a>
                                                </>
                                            )
                                                : ''}

                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <Recommend />
                                </div>

                            </div>
                        </section>
                    </div>
                </div>
                // <div>
                //     <h1>this. is my profile</h1>
                // </div>
            )

        );
    }
}

export default Profile;
