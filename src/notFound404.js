import React, { Component } from 'react';

class NotFound404 extends Component {
    constructor(params) {
        super(params)
        document.title = '404 - not found'
    }
    render() {
        return (
            this.props.usernotfound ? (
                <div>
                    <h1>404 - user not found</h1>
                </div>
            ) : (
                    <div>
                        <h1>404 - page not found</h1>
                    </div>
                )

        );
    }
}

export default NotFound404;
