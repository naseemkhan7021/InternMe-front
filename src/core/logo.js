// import './navigation.css';
import '../utilities.css'
const Logo = () => (
    <div className='footerlogo-box'>
        <a href="javasript:void(0)" className='a-deco-non'><img className="Mainlogo footerlogo" src={process.env.PUBLIC_URL + '/logo192.png'} />
        <span className='logoName'>Intern<b className='symbol'>Me</b></span></a>
    </div>
)

export default  Logo;