import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { isAuthenticate } from '../API/Autho';
import { checkUser } from '../API/Profile';
import Navigation from './Navigation';

class Home extends Component {
    constructor(params) {
        super(params)
        this.state = {
            feedRedirect: false,
        }
        document.title = 'Home | internMe'
    }
    componentDidMount() {
        if (isAuthenticate()) {
            let id = isAuthenticate().tokend;
            let token = isAuthenticate().token;
            checkUser(id, token).then(responce => {
                // console.log('reponce for isAuth -> ',responce)
                if (responce.error) {
                    console.log('erro -> ', responce.error);
                    this.setState({ feedRedirect: false })
                    return false
                }
                else if (responce.success || responce.detail) {
                    // console.log('inof ',parsinfo );
                    console.log('respoce -> ', responce);
                    this.setState({ feedRedirect: true })
                    // return responce;
                }
            })
        }
    }

    render() {
        const { feedRedirect } = this.state
        if (feedRedirect) {
            return <Redirect to={`/feed`} />
        }
        return (
            <div>
                <Navigation />
                <h1>
                    This is home page
                </h1>
            </div>
        );
    }
}

export default Home;
