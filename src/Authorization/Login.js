import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Logo from '../core/logo';
import { authenticate, isAuthenticate, login } from '../API/Autho';
import '../utilities.css';
import Footer from '../core/Footer';
import { checkUser } from '../API/Profile';

class Login extends Component {
    constructor(params) {
        super(params)
        this.state = {
            loading: false,
            error: '',
            rediractfeed: false,
            rediractToragister: false,
            id: '',
            token: '',
            email: '',
            password: '',
            userType:this.props.location.state.userType
        };
        document.title = 'Login | internMe'
    };
    showHideClick = (e) => {
        let passwordElmt = document.getElementById('password');
        if (e.target.innerText === 'Show') {
            // e.target.innerText
            passwordElmt.setAttribute('type', 'text');
            e.target.innerText = 'Hide';
        } else if (e.target.innerText === 'Hide') {
            passwordElmt.setAttribute('type', 'password');
            e.target.innerText = 'Show';
        }
    }
    handelValueChange = name => e => {
        this.buttoDisable()
        this.setState({ loading: false, error: '' });
        this.setState({ [name]: e.target.value });
        this.buttoDisable();
    }
    isValid = () => {
        const { email, password } = this.state
        if (email.length === 0) {
            this.setState({ error: 'Email is required !!', loading: false });
            return false;
        }
        if (password.length === 0) {
            this.setState({ error: 'Password is required !!', loading: false });
            return false;
        }
        this.setState({ error: '', loading: false })
        return true;
    }
    buttoDisable = () => {
        let btn = document.getElementById('submitbtn');
        if (this.isValid() === false) {
            btn.setAttribute('disabled', true)
        } else if (this.isValid() === true) {
            btn.removeAttribute('disabled')
        }
    }


    clickSubmit = (event) => {
        event.preventDefault();
        const { email, password,userType } = this.state;
        const data = { email, password };
        this.setState({ error: '', loading: true });
        if (this.isValid()) {
            login(data,userType).then(responce => {
                // console.log('login responce -> ', responce);
                if (responce.error) {
                    // console.log('login responce -> ', responce.error);
                    this.setState({ email: '', password: '', loading: false })
                    if (responce.error.notDetail === true) {
                        // console.log('this is redirect');
                        // console.log('this is redirect id -> ',responce.error.id);
                        let id = responce.error.id;
                        let token = responce.error.token;
                        var error = responce.error.error;
                        this.setState({ rediractToragister: true, id, token, error,userType:responce.error.role })

                        // this.setState({ rediractfeed: true, error: '', loading: false })
                    } else {
                        var error = responce.error.error;
                        this.setState({ error, loading: false })
                    }
                } else {
                    console.log('responce -> success -> ', responce);
                    //athentication
                    authenticate(responce.data)
                    this.setState({ rediractfeed: true, error: '', loading: false })


                }
            })
        }else{
            this.setState({loading:false})
        }
    }
    componentDidMount() {
        this.setState({loading:true})
        console.log('usertype => ',this.state.userType)
        if (isAuthenticate()) {
            let id = isAuthenticate().tokend;
            let token = isAuthenticate().token;
            checkUser(id, token).then(responce => {
                // console.log('reponce for isAuth -> ',responce)
                if (responce.error) {
                    console.log('erro -> ', responce.error);
                    this.setState({ rediractfeed: false })
                    return false
                }
                else if (responce.success || responce.detail) {
                    // console.log('inof ',parsinfo );
                    console.log('respoce -> ', responce);
                    this.setState({ rediractfeed: true })
                    // return responce;
                }
            })
        }else{
            this.setState({loading:false})
        }
    }
    render() {
        const { loading, error, rediractfeed, email, password, rediractToragister, id, token,userType } = this.state;
        if (rediractToragister) {
            return <Redirect to={`/ragisteration/${userType}/${id}-${token}?masg=${error}`} />
        }
        if (rediractfeed) {
            return <Redirect to={`/feed`} />
        }
        return (
            <div>
                <div className='bg-color d-flex text-center bg-white'>
                    <div className='mx-auto w-50'>

                        <div className='authrizeArea px-0 d-inline-block py-3'>
                            {/* singup form  */}
                            <div className='my-3 text-start'>
                                <Logo />
                            </div>
                        </div>
                        {
                            loading ? (
                                <div className="spinner-border text-danger d-block m-auto" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </div>
                            ) : ''
                        }
                        {
                            error ? (
                                // <div className="alert alert-danger alert-dismissible text-start fade show" role="alert">
                                //     <strong>Error!</strong> {error}.
                                //     <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" />
                                // </div>
                                <div className="alert alert-danger text-start" role="alert">
                                    {error}
                                </div>
                            ) : ''

                        }
                        <div id='singup' className='secundry-shadow authrizeArea'>

                            <div className='loginHeader text-start mb-4'>
                                <h2 className='first-heading m-0'>Sing in</h2>
                                <p className='small-text my-1'>Stay updated on your professional world

                                </p>
                            </div>
                            <form>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handelValueChange('email')} value={email} type="email" className="primary-input-style form-control" id="email" placeholder="name@example.com" />
                                    <label className="fw-bold font-p9" htmlFor="email">Email address</label>
                                </div>
                                <div className="form-floating">
                                    <input onChange={this.handelValueChange('password')} value={password} type="password" className=" primary-input-style form-control" id="password" placeholder="Password" />
                                    <label className="fw-bold font-p9" htmlFor="password">Password
                                    </label>
                                    <span onClick={this.showHideClick} className='showHide' id='showHide'>Show</span>
                                    <p className='first-heading m-0 text-start f-14px my-2'>
                                        <Link to={''} className='a-primary '>Forgat password?</Link></p>
                                </div>
                                {/* <span className='my-3'></span> */}

                                <button onClick={this.clickSubmit} type='submit' className='btn my-3 fw-bold authrizeArea-btn-p full-primary form-btn-width100 form-btn-radius' id='submitbtn' >Sing in</button>

                            </form>
                            <h6 className='my-3'><span className='fw-normal'>New to InternMe? </span>Join now (<Link className='a-deco-non a-primary' to={'/singup/student'}>Student</Link>/<Link className='a-deco-non a-primary' to={'/singup/employer'}>Employer</Link>) ,<span className='fw-normal'> Back to </span><Link className='a-deco-non a-primary' to={'/'}>Home</Link></h6>


                        </div>
                    </div>
                </div>
                {/* <div className='posision-footer'> */}
                <div className='container'>
                    <footer>
                        <Footer />
                    </footer>
                </div>
                {/* </div> */}
            </div>
        );
    }
}

export default Login;


