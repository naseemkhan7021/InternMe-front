import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Login from './Authorization/Login';
import PrivetRouting from './Authorization/PrivetRouting';
import Ragistration from './Authorization/Ragistrations';
import Singup from './Authorization/Singup';
import Connections from './Connections';
import Feed from './core/Feed';
import Home from './core/Home';
import HotJob from './HotJob';
// import Navigation from './core/Navigation';
import NotFound404 from './NotFound404';
import Profile from './Profile';


const AllRouter = () => (
    <div>
        {/* hare is navetions */}
        {/* <Navigation/> */}
        {/* belos is control all routs  */}
        <Switch>
            <Route exact path={['/','/home']} component={Home} />
            <PrivetRouting exact path='/feed' component={Feed} />
            <Route exact path='/singup/:userType' component={Singup} />    {/* userType -> employer or student */}
            <Route exact path='/ragisteration/:userType/:id-:token' component={Ragistration} />
            <Route exact path='/login' render={(props)=> <Ragistration {...props}/>} component={Login} />
            <Route exact path='/profile/:fname-:lname-:uid' component={Profile} />
            <Route exact path='/people' component={Connections} />
            <Route exact path='/job' component={HotJob} />
            <Route component={NotFound404} />
        </Switch>
    </div>
)
export default AllRouter;