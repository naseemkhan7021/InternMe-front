import Logo from "./logo";
import '../utilities.css';
const Footer = () => (
    <div className='bg-white vh-6 row justify-center align-items-center text-center'>
        <div className='d-flex col-md-2 col-sm-4 col-6 '>
            <Logo className='footerlogo'/>
            <p id='copymark'>&copy; 2021</p>
        </div>
        <p className='footerText col-md-2 col-sm-4 col-6'>About</p>
        <p className='footerText col-md-2 col-sm-4 col-6'>About</p>
        <p className='footerText col-md-2 col-sm-4 col-6'>About</p>
        <p className='footerText col-md-2 col-sm-4 col-6'>About</p>
        <p className='footerText col-md-2 col-sm-4 col-6'>About</p>
    </div>
)

export default Footer;