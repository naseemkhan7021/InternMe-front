/**
 * are all post show
*/

import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { isAuthenticate } from '../API/Autho';
import { checkUser, showSingluser } from '../API/Profile';
import Navigation from './Navigation';

class Feed extends Component {
    constructor(params) {
        super(params)
        this.state = {
            loading: false,
            error: '',
            redirectLogin: false,
            data: {},
            imgpic: ''
        }
        document.title = 'Feed | InternMe'
    }
    componentDidMount() {
        this.setState({ loading: true })
        if (isAuthenticate()) {
            let id = isAuthenticate().tokend;
            let token = isAuthenticate().token;
            checkUser(id, token).then(responce => {
                // console.log('reponce for isAuth -> ',responce)
                if (responce.error) {
                    // console.log('erro -> ', responce.error);
                    this.setState({ redirectLogin: true, loading: false })
                    return false
                }
                else if (responce.success || responce.detail) {
                    // console.log('inof ',parsinfo );
                    // console.log('respoce -> ', responce);
                    this.setState({ redirectLogin: false, loading: false })
                    showSingluser(id).then(responce => {
                        // console.log('singl user responce ', responce);

                        this.setState({ data: responce.user, imgpic: responce.user.profileImg.fileName })
                    })
                    // return responce;
                }
            })
        }

    }
    render() {
        const { loading, redirectLogin, data, imgpic } = this.state;
        if (redirectLogin) {
            return <Redirect to={'/login'} />
        }
        return (
            loading ? (
                <div className='min-vw-100 min-vh-100 d-flex'>
                    <div className=' m-auto'>

                        <div className="spinner-border text-danger" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>
            ) : (
                <div>
                    <header>
                        <Navigation fname={data.firstName} lname={data.lastName} unid={data.uniq} profileimg={imgpic} logdin={true} />
                    </header>
                    <div className='content-body-margin'>
                        <section>
                            <h1>This is feed</h1>
                        </section>
                    </div>
                </div>
            )
        );
    }
}

export default Feed;
